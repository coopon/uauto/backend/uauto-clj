# Ride Acknowledgement
- I should able to know if my ride has been accepted by a driver
- I should be able to know my ride & driver's details
- I should be able to know my driver's real time location
- I should know when the driver has arrived
- I should be able to share my ride with my friends

# Ride Acceptance
- I should be able to specify if I am 
    willing to accept rides or not
- I should be able to receive ride requests
- I should know the incoming ride details - source, drop, est {fare / duration}
- I should be able to accept or decline the ride 

# Ride Happening
- I should be able to see passengers' details
- I should be able to see GPS routing to pickup point
- I should notify passenger of my arrival {manual & GPS}
- I should be able to start the ride
- I should be able to send SOS during ride
- I should be able to see intermediary destinations
- I should be able to confirm arrival at destination, initiate payment
- I should be able to confirm receipt of payment
- I should be able to confirm end of ride

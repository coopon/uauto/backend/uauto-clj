(ns uauto.site.auth
  (:require [buddy.hashers :as hashers]
            [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [uauto.site :as site]
            [uauto.db.users :as users]
            [uauto.db :as db]))

(defn user-by-email-exists?
  "Checks if user for the given email exists"
  [email]
  (let [data (users/users-by-email db/jdbc email)]
    (nil? data)))

(defn new-user
  "Creates a new user in the db"
  [data]
  (println data)
  (let [datum
        {:email (:email data)
         :password (hashers/derive (:password data) {:alg :bcrypt+blake2b-512})}]
    (println datum)
    (if (user-by-email-exists? datum)
      (users/users-create db/jdbc datum)
      (println "User exists"))))
